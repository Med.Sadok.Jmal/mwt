package com.jms.mwt.repository;

import com.jms.mwt.entity.AppUser;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

@Repository
@Transactional
public class UserRepositoryImpl implements UserRepositoryCustom {

    @PersistenceContext(name = "mwtPU")
    private EntityManager entityManager;


    @Override
    public AppUser findUserByEmail(String email) {
        try{
            Query query = entityManager.createNativeQuery(
                    "SELECT user.* FROM mwt.user as user WHERE user.email = :email",
                    AppUser.class
            );
            query.setParameter("email", email);
            return (AppUser) query.getSingleResult();
        } catch(NoResultException e) {
            return null;
        }
    }

    @Override
    public AppUser loadUser(String email, String password) {
        Query query = entityManager.createNativeQuery(
                "SELECT user.* FROM mwt.user as user WHERE user.email = :email",
                AppUser.class
        );
        query.setParameter("email", email);
        return (AppUser) query.getSingleResult();
    }
}
