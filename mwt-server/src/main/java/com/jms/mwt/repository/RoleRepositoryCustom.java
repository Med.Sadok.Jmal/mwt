package com.jms.mwt.repository;

import com.jms.mwt.entity.AppRole;

public interface RoleRepositoryCustom {

    AppRole findRoleByName(String name);
}
