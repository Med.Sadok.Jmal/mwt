package com.jms.mwt.repository;

import com.jms.mwt.entity.AppUser;

public interface UserRepositoryCustom {

    AppUser findUserByEmail(String email);
    AppUser loadUser(String email, String password);
}
