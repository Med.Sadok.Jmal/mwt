package com.jms.mwt.repository;

import com.jms.mwt.entity.Trip;

import java.util.List;

public interface TripRepositoryCustom {

    void addTripToUser(Trip trip, Long userId);

    List<Trip> getAllTripsByUserId(Long id);
}
