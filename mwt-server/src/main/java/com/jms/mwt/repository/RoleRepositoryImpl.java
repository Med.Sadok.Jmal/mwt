package com.jms.mwt.repository;

import com.jms.mwt.entity.AppRole;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

@Repository
@Transactional
public class RoleRepositoryImpl implements RoleRepositoryCustom {

    @PersistenceContext(name = "mwtPU")
    private EntityManager entityManager;

    @Override
    public AppRole findRoleByName(String name) {
        Query query = entityManager.createNativeQuery(
                "SELECT role.* FROM mwt.role as role WHERE role.name = ?",
                AppRole.class
        );
        query.setParameter(1, name);
        return (AppRole) query.getSingleResult();
    }
}
