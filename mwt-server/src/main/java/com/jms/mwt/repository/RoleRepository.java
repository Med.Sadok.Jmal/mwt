package com.jms.mwt.repository;

import com.jms.mwt.entity.AppRole;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RoleRepository extends CrudRepository<AppRole, Long>, RoleRepositoryCustom {
}
