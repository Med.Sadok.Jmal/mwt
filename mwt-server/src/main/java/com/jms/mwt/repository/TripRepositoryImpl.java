package com.jms.mwt.repository;

import com.jms.mwt.entity.Trip;
import com.jms.mwt.entity.AppUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;
import java.util.Optional;

@Repository
@Transactional
public class TripRepositoryImpl implements TripRepositoryCustom {

    @PersistenceContext(name = "mwtPU")
    private EntityManager entityManager;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private TripRepository tripRepository;

    @Override
    public void addTripToUser(Trip trip, Long userId) {
        Optional<AppUser> optionalUser = userRepository.findById(userId);
        optionalUser.ifPresent(
                user -> {
                    trip.setUser(user);
                    tripRepository.save(trip);
                }
        );
    }


    @Override
    public List<Trip> getAllTripsByUserId(Long userId) {
        Query query = entityManager.createQuery(
                "SELECT trip.* FROM mwt.trip JOIN mwt.user user ON user.id = trip.user_id WHERE user.id = :userId",
                Trip.class
        );
        return (List<Trip>) query.getResultList();
    }
}
