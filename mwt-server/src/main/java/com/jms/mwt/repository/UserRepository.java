package com.jms.mwt.repository;

import com.jms.mwt.entity.AppUser;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public interface UserRepository extends CrudRepository<AppUser, Long>, UserRepositoryCustom {
}
