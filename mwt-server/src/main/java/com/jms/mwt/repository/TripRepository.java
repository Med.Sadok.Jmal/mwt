package com.jms.mwt.repository;

import com.jms.mwt.entity.Trip;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TripRepository extends CrudRepository<Trip, Long>, TripRepositoryCustom {
}
