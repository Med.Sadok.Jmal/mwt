package com.jms.mwt.service;

import com.jms.mwt.entity.AppUser;
import com.jms.mwt.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    public void save(AppUser user) {
        userRepository.save(user);
    }

    @Override
    public AppUser findUserByEmail(String email) {
        return userRepository.findUserByEmail(email);
    }
}
