package com.jms.mwt.service;

import com.jms.mwt.entity.Task;

import java.util.List;

public interface TaskService {

    List<Task> findAllTasks();
    Task addTask(Task task);
}
