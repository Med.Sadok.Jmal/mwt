package com.jms.mwt.service.initializer;

import com.jms.mwt.entity.AppRole;
import com.jms.mwt.entity.AppUser;
import com.jms.mwt.entity.Task;
import com.jms.mwt.service.AccountService;
import com.jms.mwt.service.TaskService;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.stream.Stream;

@Service
public class InitBean implements InitializingBean {

    @Autowired
    private TaskService taskService;

    @Autowired
    private AccountService accountService;

    @Override
    public void afterPropertiesSet() throws Exception {
        // Add tasks
        Stream.of("task1", "task2", "task3").forEach(task -> {
            taskService.addTask(new Task(task));
        });
        taskService.findAllTasks().forEach(task -> {
            System.out.println(task.getName());
        });

        // users
        AppUser user1 = new AppUser("med@gmail.com", "1234");
        user1.setPhoneNumber("34567788");
        user1.setFirstName("med");
        accountService.saveUser(user1);
        AppUser user2 = new AppUser("marie@gmail.com", "0000");
        user2.setPhoneNumber("34567788");
        user2.setFirstName("med");
        accountService.saveUser(user2);

        // roles
        AppRole role1 = new AppRole("ADMIN");
        AppRole role2 = new AppRole("USER");
        accountService.saveRole(role1);
        accountService.saveRole(role2);

        // add roles to users
        accountService.addRoleToUser("med@gmail.com", "ADMIN");
        accountService.addRoleToUser("med@gmail.com", "USER");
        accountService.addRoleToUser("marie@gmail.com", "USER");


    }
}
