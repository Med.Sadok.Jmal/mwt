package com.jms.mwt.service;

import com.jms.mwt.entity.AppUser;

public interface UserService {

    void save(AppUser user);

    AppUser findUserByEmail(String email);
}
