package com.jms.mwt.service;

import com.jms.mwt.entity.Task;
import com.jms.mwt.repository.TaskRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TaskServiceImpl implements TaskService {

    @Autowired
    private TaskRepository taskRepository;

    public List<Task> findAllTasks() {
        return (List<Task>) taskRepository.findAll();
    }

    public Task addTask(Task task) {
        return taskRepository.save(task);
    }
}
