package com.jms.mwt.service;

import com.jms.mwt.entity.Trip;

import java.util.List;

public interface TripService {

    Trip save(Trip trip);

    void addTripToUser(Trip trip, Long userId);

    List<Trip> findAllTrips();
}
