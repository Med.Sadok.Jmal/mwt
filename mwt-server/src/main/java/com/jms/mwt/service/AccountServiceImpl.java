package com.jms.mwt.service;

import com.jms.mwt.entity.AppRole;
import com.jms.mwt.entity.AppUser;
import com.jms.mwt.repository.RoleRepository;
import com.jms.mwt.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class AccountServiceImpl implements AccountService {

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Override
    public AppUser saveUser(AppUser user) {
        String hashedPassword = bCryptPasswordEncoder.encode(user.getPassword());
        user.setPassword(hashedPassword);
        return userRepository.save(user);
    }

    @Override
    public AppRole saveRole(AppRole role) {
        return roleRepository.save(role);
    }

    @Override
    public void addRoleToUser(String email, String roleName) {
        AppRole role = roleRepository.findRoleByName(roleName);
        AppUser user = userRepository.findUserByEmail(email);
        user.getRoles().add(role);
    }

    @Override
    public AppUser findUserByEmail(String email) {
        return userRepository.findUserByEmail(email);
    }
}
