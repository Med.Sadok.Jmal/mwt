package com.jms.mwt.service;

import com.jms.mwt.entity.Trip;
import com.jms.mwt.repository.TripRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class TripServiceImpl implements TripService {

    @Autowired
    private TripRepository tripRepository;

    @Override
    public Trip save(Trip trip) {
        return(tripRepository.save(trip));
    }

    @Override
    public void addTripToUser(Trip trip, Long userId){
        tripRepository.addTripToUser(trip, userId);
    }

    @Override
    public List<Trip> findAllTrips() {
        return (List<Trip>) tripRepository.findAll();
    }
}
