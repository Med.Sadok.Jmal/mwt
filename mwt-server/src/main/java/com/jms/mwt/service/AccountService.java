package com.jms.mwt.service;

import com.jms.mwt.entity.AppRole;
import com.jms.mwt.entity.AppUser;

public interface AccountService {

    AppUser saveUser(AppUser user);
    AppRole saveRole(AppRole role);
    void addRoleToUser(String email, String roleName);
    AppUser findUserByEmail(String email);
}
