package com.jms.mwt.controller;

import com.jms.mwt.entity.Task;
import com.jms.mwt.service.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/api/v1")
public class TaskRestController {

    @Autowired
    private TaskService taskService;

    @GetMapping("/tasks")
    public List<Task> getAllTasks() {
        return taskService.findAllTasks();
    }

    @PostMapping("/tasks")
    public Task save(@RequestBody Task task) {
        return taskService.addTask(task);
    }

}
