package com.jms.mwt.controller;

import com.jms.mwt.entity.Message;
import com.jms.mwt.entity.Product;
import com.jms.mwt.entity.Trip;
import com.jms.mwt.service.TripService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.*;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/api/v1")
public class HelloWorldRestController {

    @Autowired
    private TripService tripService;

    @RequestMapping("/")
    public String welcome() {//Welcome page, non-rest
        return "Welcome to RestTemplate Example.";
    }

    @RequestMapping("/hello/{player}")
    public Message message(@PathVariable String player) {//REST Endpoint.

        return new Message(player, "Hello " + player);
    }

    @GetMapping("/messages")
    public List<Message> getAllMessages() {
        System.out.println("Get all messages...");

        List<Message> messages = new ArrayList<>();
        messages.add(new Message("marie", "salut bibi"));

        return messages;
    }

    @GetMapping("/products")
    public List<Product> getAllProducts() {
        System.out.println("Get all products...");

        List<Product> products = new LinkedList<>();

        Product product1 = new Product();
        product1.setProductId(1);
        product1.setProductName("Leaf Rake");
        product1.setProductCode("GDN-0011");
        product1.setReleaseDate("March 19, 2016");
        product1.setDescription("Leaf rake with 48-inch wooden handle.");
        product1.setPrice(19.95f);
        product1.setStarRating(3.2f);
        product1.setImageUrl("https://openclipart.org/image/300px/svg_to_png/26215/Anonymous_Leaf_Rake.png");
        products.add(product1);

        Product product2 = new Product();
        product2.setProductId(2);
        product2.setProductName("Garden Cart");
        product2.setProductCode("GDN-0023");
        product2.setReleaseDate("March 18, 2016");
        product2.setDescription("15 gallon capacity rolling garden cart.");
        product2.setPrice(32.99f);
        product2.setStarRating(4.2f);
        product2.setImageUrl("https://openclipart.org/image/300px/svg_to_png/58471/garden_cart.png");
        products.add(product2);

        Product product10 = new Product();
        product10.setProductId(10);
        product10.setProductName("Video Game Controller");
        product10.setProductCode("GMG-0042");
        product10.setReleaseDate("October 15, 2015");
        product10.setDescription("Standard two-button video game controller.");
        product10.setPrice(35.95f);
        product10.setStarRating(4.6f);
        product10.setImageUrl("ttps://openclipart.org/image/300px/svg_to_png/120337/xbox-controller_01.png");
        products.add(product10);

        Product product8 = new Product();
        product8.setProductId(8);
        product8.setProductName("Saw");
        product8.setProductCode("TBX-0022");
        product8.setReleaseDate("March 15, 2016");
        product8.setDescription("15-inch steel blade hand saw.");
        product8.setPrice(11.55f);
        product8.setStarRating(3.7f);
        product8.setImageUrl("https://openclipart.org/image/300px/svg_to_png/27070/egore911_saw.png");
        products.add(product8);

        Product product5 = new Product();
        product5.setProductId(5);
        product5.setProductName("Hammer");
        product5.setProductCode("TBX-0048");
        product5.setReleaseDate("March 21, 2016");
        product5.setDescription("Curved claw steel hammer.");
        product5.setPrice(8.9f);
        product5.setStarRating(4.8f);
        product5.setImageUrl("https://openclipart.org/image/300px/svg_to_png/73/rejon_Hammer.png");
        products.add(product5);

        Trip trip = new Trip("Norway");
        trip.setStartDate(LocalDate.parse("2017-11-15"));
        trip.setEndDate(LocalDate.parse("2017-11-23"));
        //tripService.save(trip);
        tripService.addTripToUser(trip, 21L);

        return products;
    }

    @GetMapping("/products/{id}")
    public Product getProduct(@PathVariable("id") long id) {
        return new Product();
    }

    @PostMapping
    @ResponseStatus(HttpStatus.OK)
    public void createProduct(@RequestBody Product product) {

    }
}
