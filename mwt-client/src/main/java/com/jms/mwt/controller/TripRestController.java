package com.jms.mwt.controller;

import com.jms.mwt.entity.Trip;
import com.jms.mwt.entity.AppUser;
import com.jms.mwt.service.TripService;
import com.jms.mwt.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/api/v1")
public class TripRestController {

    @Autowired
    private TripService tripService;

    @Autowired
    private UserService userService;

    @GetMapping("/trips")
    public List<Trip> findAllTrips() {
        // userService.save(user);

        // find user
        AppUser user = userService.findUserByEmail("med@gmail.com");

        tripService.addTripToUser(new Trip("Bali"), user.getId());
        // end to delete
        return tripService.findAllTrips();
    }

}
