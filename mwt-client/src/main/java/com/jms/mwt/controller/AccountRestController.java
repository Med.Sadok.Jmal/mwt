package com.jms.mwt.controller;

import com.jms.mwt.entity.AppUser;
import com.jms.mwt.model.RegisterForm;
import com.jms.mwt.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/api/v1")
public class AccountRestController {

    @Autowired
    private AccountService accountService;

    @PostMapping("/register")
    public AppUser register(@RequestBody RegisterForm registerForm) {
        // Verify if the passwords match
        if (!registerForm.getPassword().equals(registerForm.getConfirmedPassword())) {
            throw new RuntimeException("Unmatched password!");
        }

        // Verify if user already exists
        AppUser user = accountService.findUserByEmail(registerForm.getEmail());
        if (user != null) {
            throw new RuntimeException("User already exists!");
        }

        // Save new User
        user = new AppUser(registerForm.getEmail(), registerForm.getPassword());
        return accountService.saveUser(user);
    }

}
